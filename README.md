### Java工具：源代码抽取

- 从项目中提取小应用（或子模块）
- 理想情形下，所提出的代码经小幅修改后可独立使用。
- 可借此抽取代码或“删除无用的代码”。
- 这些事若依靠人力完成，会很辛苦。

### 说明：

1.	当前状态：测试、编译自jdk1.8。
2.	技术原理：借助于asm字节码工具，从入口类（比如提供main函数的那个类）开始，递归深入其各个方法中的代码元素，并生成引用图。然后提取引用图中出现的所有代码元素， 将其写入到指定的源代码目录中。
代码元素指：属性、方法、注解、类等。
3.	因未能掌握代码的实际运行时路径，这样的提取不完美，但实用即可。
4.	原则上不提取未被访问到的属性和方法。
5.	以后的版本或可根据需要，实现对多次执行的结果进行积累合并，不断地“提取”代码，直到满意为止；或借助日志深入运行时路径，提取更为精准的内容。



### 安装、使用

- 若对源码没兴趣，可直接取用lib目录下的jar文件和配置文件lib/_a/*.java
- 使用简单，请参见：readme_Java源代码抽取工具_2021.docx

--------------  END. --------------

