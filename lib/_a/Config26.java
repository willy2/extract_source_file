package _a;
import java.io.File;
import java.io.FileFilter;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import mathy.c.Coms26a;
import mathy.wili.c.MiscFilter.ClsFilter;
import mathy.wili.c.asm.AsmItems;
import mathy.wili.db.misc.MethodMap;
import net.bytebuddy.jar.asm.ClassVisitor;
import net.bytebuddy.jar.asm.MethodVisitor;
//import wili.c.asm.AsmItems;
//import _j.CodeExtract_byAsm;
/**
 * 
 * @author weila 2021年2月14日 
 */
public interface Config26 {//ExtractConf
	public static final boolean LogOn = "".isEmpty();

	/**
	 * Give a directory as extract destination.
	 */
	File Extract_toSourceDir = new File("C:/t/CollectCode/src");

	static int OPT = "".isEmpty() ? 1 : -1;
	/**
	 * 	工具将在这些目录中提取源代码。
	 */
	public static File[] getRootDirs_of_sourceCode() {
		if (OPT == 1 || OPT == 2) {
			File root = new File("D:/bearJ/autoeq2/src/main");
			return root.listFiles(new FileFilter() {
				@Override
				public boolean accept(File pathname) {
					return pathname.isDirectory() && pathname.getName().startsWith("src");
				}
			});
		} else if (OPT == 3)
			return new File[] { new File("C:\\Users\\weila\\eclipse-workspace\\test3\\src") };
		return null;
	}

	/**
	 * Give a class to start extraction (may be class with main method)
	 */
	public static Class<?> getStartClass() {
		if (OPT == 1)
			return Object.class;
		else if (OPT == 2)
			return mathy.wili.c.asm.FindPutFieldFromClassFile.class;
		return null;
	}
	/**
	 * 	Aapp data directory.
	 * 	必要的数据文件写入此目录，用户无需关心，必要时或需手动清除其中文件。
	 */
	File ConfRoot = new File("c:/t/insertLog");

	/**
	 *   Interface or abstract class may have some posibble implementation... 
	 */
	public int MaxImpleClassNum = 3;

	/**
	 * 	重要：因难以知晓运行时jar中接口所使用或回调的具体类，用户当在此列出重要的接口，确保其实现类被全面访问提取。
	 */
	static final List<Class<?>> ClsSet_whichSubClsTobeFullyVisited = Arrays.asList(ClassVisitor.class,
			MethodVisitor.class, AsmItems.class);

	/**
	 *  Copy these file if it is visited.
	 */
	Set<Class<?>> FILES_tobe_copied = new HashSet<>(Arrays.asList(//
	));

	/**
	 * 	Copy files in these package if it is visited.
	 */
	List<String> PACKAGE_tobe_copied = Arrays.asList(//
	//Java8Lexer.class.getPackage().getName(), //
	//Visit26.class.getPackage().getName()
	//
	);

	/**
	 * Never extract un-accepted class(eg. class in jar file)
	 */
	ClsFilter Class_filter = Coms26a.Class_filter;

	/**
	 * 	因工具遗漏，特强制要求提取这些方法。
	 */
	MethodMap TobeExtracted_methods = new MethodMap().add("toString").add("compareTo", Object.class);

	/**
	 * 	统计并提示所引用的jar包（有较大耗时）
	 */
	boolean ExploreJarClass = "".isEmpty();
	public static void main(String[] args) {
		CodeExtract_start.startExtractFromClass(Config26.getStartClass());
	}
}