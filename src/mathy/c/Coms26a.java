package mathy.c;
import java.io.File;
import java.util.regex.Pattern;
import _a.Config26;
import mathy.wili.c.Class9;
import mathy.wili.c.MiscFilter.ClsFilter;
import mathy.wili.extract_srcFile.java.Cont26;
import mathy.wili.insertLog.JarFilePackageNameSet;
import mathy.wili.misc.Time9;
/**
 * 
 * @author weila 2021-2-9
 */
public class Coms26a {
	private static Pattern pkgSetPat;
	public static boolean isIn_tobeCopiedPackage(String clsName) {
		if (pkgSetPat == null) {
			if (Config26.PACKAGE_tobe_copied.isEmpty())
				return false;
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < Config26.PACKAGE_tobe_copied.size(); i++) {
				String st = Config26.PACKAGE_tobe_copied.get(i);
				if (i > 0)
					sb.append('|');
				sb.append(st);
				if (!st.endsWith("."))
					sb.append("\\.");
			}
			pkgSetPat = Pattern.compile(sb.toString());
		}
		if (clsName.charAt(0) == '.')
			clsName = clsName.substring(1);
		return pkgSetPat.matcher(clsName).lookingAt();
	}
	public static ClsFilter Class_filter = new ClsFilter() {
		/**
		 * Never extract un-accepted class(eg. class in jar file)
		 */
		@Override
		public boolean accept(Class<?> cls) {
			if (cls.isArray() || cls.isPrimitive())
				return false;
			String name = cls.getPackageName();
			if (name.startsWith("java.") || name.startsWith("javax."))
				return false;
			if (!Config26.ExploreJarClass && JarFilePackageNameSet.contains(name))
				return false;
			if ("".isEmpty()) {//一次扫描后就不需要重复检查了.
				if (Boolean.FALSE)
					Time9.time(1, "CLASS_Filter");
				File file = Class9.jarFileOrClassRootOf(cls);//slow.
				if (file == null || file.isFile()) {
					if (file != null)
						Cont26.JarFileList.add(file.getAbsolutePath());
					JarFilePackageNameSet.add(name);
					return false;//class in jar file.
				}
			}
			return true;
		}
	};
}
