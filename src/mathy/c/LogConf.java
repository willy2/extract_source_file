package mathy.c;
import java.io.File;
import java.io.FileFilter;
import mathy.wili.c.Str9;
/**
 * 	Use configuration.
 * @author weila 2021年1月9日
 */
public class LogConf {
	public static File ConfRoot = new File("c:/t/insertLog");

	static final String NL = Str9.NL;

	public static final int PORT = 443;

	public static final String IdName = "id_weilai345";

	public static final String Dent = "   ";

	public static final int MAX_CLS_NUM = 10000;
	/**
	 * ... find java file from dirs.
	 */
	public static File[] getRootDirs_findFile() {
		File root = new File("D:/bearJ/autoeq2/src/main");
		return root.listFiles(new FileFilter() {
			@Override
			public boolean accept(File pathname) {
				return pathname.isDirectory() && pathname.getName().startsWith("src");
			}
		});
	}
	public static final int PrintString_maxLength = 100;

	public static final int PrintSet_maxSize = 20;//打印有少量元素的集合

	public static final int MaxDent = 20;
}