package mathy.wili.al.antlr.java8.coda;
import mathy.c.Ca;
public enum AppName {
	run, log, collectCode, collectUI;
	private static AppName CurApp;
	public static void setApp(AppName app) {
		app.setMe();
	}

	public static void setApp_collectCode() {
		setApp(collectCode);
	}

	public static void setApp_log() {
		setApp(log);
	}

	public static boolean isNull() {
		return CurApp == null;
	}

	private void setMe() {
		if (CurApp != null && CurApp != this) {
			String msg = "Already ConfJ.AppName=" + CurApp;
			if ("asfd".isEmpty())
				Ca.asert(false, msg);
			Ca.debug(1, msg);
			return;
		}
		CurApp = this;
	}

	public boolean isMe() {
		if (CurApp.name().equals(this.name()))
			return true;
		return false;
	}
}