package mathy.wili.c;
import mathy.c.Ca;
import mathy.wili.c.types.IsRet;
/**
 * @author willy2, 2011-3-13
 */
public class C2 {// 中国字
	//
	static final String OS_NAME = System.getProperty("os.name");//Windows,Linux

	public static final boolean IsWindows = OS_NAME.startsWith("Windows");

	// public static boolean DEBUG1 = true;
	public static final String DIR = "d:/bear";

	public static final boolean TRUE = Boolean.TRUE;

	public static final boolean FALSE = Boolean.FALSE;

	public static final String NL = "\r\n";
	public static void pause() {
	}

	/** Throw exception if !isTrue */
	public static void asert(boolean isTrue) {
		asert(isTrue, "[]");
	}
	private static long asertId;

	public static int errNum = 0;
	public static void asert(boolean is, Object msg) {
		if (!is) {
			if (++asertId == 2)
				C2.pause();
			++errNum;
			String asId = ", asertID=";
			if (msg instanceof Exception) {
				((Exception) msg).printStackTrace();
				throw new AsertException(((Exception) msg).getMessage() + asId + asertId, (Exception) msg);
			}
			AsertException ex = new AsertException("\n" + msg + asId + asertId + ":" + IsRet.GenID, null);
			ex.printStackTrace();
			throw ex;
		}
		C2.pause();
	}
	static class AsertException extends RuntimeException {
		public AsertException(String msg, Exception e) {
			super(msg, e);
		}
		private static final long serialVersionUID = 1L;
	}
	public static void log(int opt, Object... msgs) {
		if (opt < 0)
			return;
		if ("".isEmpty()) {
			Log9.print(opt, msgs);
			return;
		}
		String st = Misc9.strOfObjs("", msgs);
		log00(st);
		if (st.contains("failed."))
			C2.pause();
		if (incStep == 99)
			Ca.pause();
	}

	private static void log00(Object msg) {
		//日志级别：OFF、ALL1、FATAL6、ERROR5、WARN4、INFO 3、DEBUG2
		if (msg == null)
			System.out.println(msg);
		String st = msg.toString();
		if (st.contains("1, 93:<214748364") || st.contains("18.0,"))
			C2.pause();
		if (SKIP_LOG)
			return;
		System.out.println(st);
	}
	public static long incStep;

	public static boolean SKIP_LOG = false;
}
