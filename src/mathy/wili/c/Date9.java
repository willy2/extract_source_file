package mathy.wili.c;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
@SuppressWarnings("deprecation")
public class Date9 {
	final static String yyyyMMdd = "yyyy-MM-dd";

	final static String yyyyMMdd_HHmmss = "yyyy-MM-dd HH:mm:ss";

	final static String HHmmss = "HH:mm:ss";

	final static String yyyyMMdd_HHmmss_SSS = "yyyy-MM-dd HH:mm:ss.SSS";

	final static String yyyyMMddHHmmss = "yyyyMMddHHmmss";
	/**
	 * yyyyMMdd_HHmmss
	 */
	public static String dateTimeStrOfNow() {
		return dateStrOf(new Date(), yyyyMMdd_HHmmss);
	}

	/**
	 * @return eg. 2018-10-15 09:11:13 
	 */
	public static String dateTimeStrOf(long timeInMillis) {
		return dateStrOf(new Date(timeInMillis), yyyyMMdd_HHmmss);
	}

	/** e.g. format="yyyy-MM-dd HH:mm:ss" */
	private static String dateStrOf(Date date, String format) {
		SimpleDateFormat df = new SimpleDateFormat(format, Locale.CHINA);
		String ret = df.format(date);
		return ret;
	}

	public static String toString(Calendar cal) {
		return new Date(cal.getTimeInMillis()).toLocaleString();
	}
}
