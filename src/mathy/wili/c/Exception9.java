package mathy.wili.c;
/**
 * 
 * @author weila 2020年3月10日
 */
public class Exception9 {
	//	public static String linkString_of(Class<?> cls, int lineNo) {//打印到控制台后，是超链接
	//		String st = "(" + cls.getSimpleName() + ".java:" + lineNo + ")";
	//		return "at " + st;//at 前需有空格，否则超连接失效
	//	}
	public static String linkString_of(Class<?> cls, int lineNo, String msg) {//打印到控制台后，是超链接
		String st = "(" + cls.getSimpleName() + ".java:" + lineNo + ")";
		return Exception.class.getName() + ": " + msg + "\n\tat " + st;
	}
}
