package mathy.wili.c;
import mathy.c.Ca;
/**
 * 
 * @author weila 2021-2-7
 */
public class Log9 {
	static final boolean UseCache = "abc".isEmpty();

	static final StringBuilder SB = "456".isEmpty() ? null : new StringBuilder(1000);
	public static void print(int use, Object... msgs) {
		if (use < 0)
			return;
		if (msgs.length == 0)
			return;
		String ret = SB == null ? ret = Ca.strOfObjs(msgs) : append(SB, false, msgs);
		Ca.pause();//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
		if (SB == null) {
			System.out.println(ret);
		} else if (!UseCache || SB.length() > 1000) {
			System.out.println(SB.toString());
			SB.setLength(0);//2 times faster than System.out.println();
		}
	}

	static String append(StringBuilder sb, boolean newLine, Object... aa) {
		if (newLine)
			sb.append('\n');
		int start = sb.length();
		for (Object obj : aa) {
			sb.append(obj);
		}
		return sb.substring(start, sb.length());
	}
}
