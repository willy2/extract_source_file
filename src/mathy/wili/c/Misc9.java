package mathy.wili.c;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import mathy.c.Ca;
//jdk7
/**
 * Miscellanous functions.
 * 
 * @author weilai
 */
@SuppressWarnings("all")
public class Misc9 {
	static final String NL = Str9.NL;

	static final String NL1 = "\n", NULL = "null";

	static final String sups = "⁰¹²³⁴⁵⁶⁷⁸⁹", Jj = "±";// 特殊符号

	static final String GREEK_LETTER = "θαβγλμωεπ";
	public static <T> String concatList(Collection list, String separator) {
		StringBuilder sb = new StringBuilder(list.size() * 4);
		int ind = -1;
		boolean useLineNo = separator.indexOf('\n') != -1;
		for (Object ele : list) {
			String st = ele.toString();
			st = st.trim();
			if (++ind > 0) {
				sb.append(separator);
				if (useLineNo)
					sb.append(ind + ",\t");
			}
			sb.append(st);
		}
		return sb.toString();
	}

	/**
	 * Return the lineNo at given location, first lineNo is 0,...
	 * @param index
	 * @param lineNoList
	 * @see this{@link #indexListOfLines(String)}
	 * @return
	 */
	public static int lineNoAt(int index, List<Integer> lineNoList) {
		if (Boolean.FALSE) {
			indexListOfLines("");
		}
		int ret = Collections.binarySearch(lineNoList, index);//6930， ind=6932
		if (ret >= 0)
			return ret + 1;
		return -ret;//若插入点是n, 从零计数的行号就是n
	}

	/**
	 * if x>1, Line x is txt.substring(inds[i-2],inds[i-1])<br>
	 * line 1 is txt.substring(0,inds[0])
	 * @return  inds.
	 * @see this{@link #lineNoAt(int, List)}
	 */
	public static List<Integer> indexListOfLines(String txt) {
		List<Integer> lineNoList = new ArrayList<Integer>(64);
		int ind = 0;
		String NL2 = System.lineSeparator();
		if ("".isEmpty()) {
			Pattern nlPat = Pattern.compile(System.lineSeparator() + "|\n|\r");//java编辑器就是这么分行的
			Matcher mat = nlPat.matcher(txt);
			int from = 0;
			int times = 0;
			while (mat.find()) {
				//System.out.println(++times + ":" + text.substring(from, mat.start()));
				lineNoList.add(mat.start());
				if (times == 99)
					Ca.pause();
				from = mat.end();
			}
			return lineNoList;
		}
		int from = 0, times = 0;
		while ((ind = txt.indexOf(NL1, ind)) != -1) {
			String st = txt.substring(from, ind).toString();
			System.out.println(++times + ":" + st);
			int valu = (int) txt.charAt(ind + 3);
			if (times == 99)
				Ca.pause();
			lineNoList.add(ind);
			ind += NL1.length();
			from = ind;
		}
		System.out.println(++times + ":" + txt.substring(from).toString());
		return lineNoList;
	}

	public static List<Double> listOfNums(double... dd) {
		List<Double> list = new ArrayList<Double>(dd.length);
		for (double d : dd) {
			list.add(d);
		}
		return list;
	}
	static final String yyMMdd_HHmmssStr = "yyyy-MM-dd HH:mm:ss";

	static final String yyMMdd_HHmmssStrSSS = "yyyy-MM-dd HH:mm:ss.SSS";
	public static String strOfList(Collection list, String separator) {
		boolean hasLineNo = separator.startsWith("\n");
		StringBuilder sb = new StringBuilder(list.size() * 16);
		int ind = 0;
		for (Object ele : list) {
			//for (int i = 0; i < list.size(); i++) {
			if (++ind > 1 || hasLineNo)
				sb.append(separator);
			if (hasLineNo) {
				//String.format("%1$-3s", ind)
				sb.append(ind + ",");
			}
			if (ele == null) {
				sb.append(ele);
			} else if (ele instanceof Object[]) {
				Object[] aa = (Object[]) ele;
				List lis = Arrays.asList(aa);
				sb.append(lis);
			} else if (ele instanceof double[]) {
				double[] aa = (double[]) ele;
				List lis = Misc9.listOfNums(aa);
				sb.append(lis);
			} else {
				sb.append(ele.toString());
			}
		}
		return sb.toString();
	}

	public static String strOfList(Collection list, int showLineNo, String separator) {
		return strOfList(list, separator);
	}
	public static interface Color {
		/*
		 * black red green brown blue magenta cyan white underscoreOn underscoreOff ...
		 * see: Eclipse/MyEclipse在线安装 ANSI Escape in Console 插件...
		 */
		String Deft = "\033[0m", Bold = "\033[1m", Blink = "\033[5m";

		String Black = "\033[30m", Red = "\033[31m", Green = "\033[32m", Brown = "\033[33m";

		String Blue = "\033[34m", Magenta = "\033[35m", Cyan = "\033[36m", White = "\033[37m";

		//
		String Black2 = "\033[40m", Red2 = "\033[41m", Green2 = "\033[42m", Brown2 = "\033[43m";

		String Blue2 = "\033[44m", Magenta2 = "\033[45m", Cyan2 = "\033[46m", White2 = "\033[47m";
	}
	public static void printList(Collection cc, int showLineIndex) {
		if (cc instanceof TreeSet) {
		} else if (cc instanceof Set) {
			cc = new TreeSet<>(cc);
		}
		StringBuilder sb = new StringBuilder();
		int lineInd = 0;
		for (Object value : cc) {
			++lineInd;
			if (sb.length() > 0)
				sb.append('\n');
			String keySt;
			String valueSt = value == null ? "Null" : value.toString();
			if (showLineIndex == 1) {
				keySt = String.format("%1$4s", lineInd);
				sb.append(keySt + ": " + valueSt);
			} else {
				sb.append(valueSt);
			}
		}
		C2.log(1, sb);
	}
	public static final String CompileErr = "CompileErr";
	public static String strOfObjs(String deli, Object[] objs) {
		if (objs.length == 0)
			return "";
		if (objs.length == 1)
			return String.valueOf(objs[0]);
		if (objs.length == 2)
			return String.valueOf(objs[0]) + String.valueOf(objs[1]);
		int size = 0;
		for (Object ele : objs)
			size += String.valueOf(ele).length();
		StringBuilder sb = new StringBuilder(size);
		for (int i = 0; i < objs.length; i++) {
			if (!deli.isEmpty() && i > 0)
				sb.append(deli);
			sb.append(String.valueOf(objs[i]));
		}
		return sb.toString();
	}
}