package mathy.wili.c;
/**
 * 
 * @author weila 2021年2月15日
 */
public interface MiscFilter {
	interface ClsFilter {
		boolean accept(Class<?> cls);
	}
	interface ObjFilter {
	}
}
