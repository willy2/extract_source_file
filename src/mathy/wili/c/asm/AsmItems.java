package mathy.wili.c.asm;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Executable;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import mathy.c.Ca;
import mathy.wili.c.Class9;
import mathy.wili.extract_srcFile.java.FieldRange;
/**
 * 
 * @author weila 2021年3月1日
 */
public abstract class AsmItems {
	public final int id = ++GenID;

	public final List<AsmItem> list = new ArrayList<>();

	public final AsmItem fromItem;
	public AsmItems(AsmItem preItem) {
		this.fromItem = preItem;
	}

	public void print(int opt) {
		if (opt < 0)
			return;
		System.out.println("---------" + AsmItems.class.getSimpleName() + ":");
		for (int i = 0; i < list.size(); i++) {
			AsmItem item = list.get(i);
			System.out.println(item);
		}
	}

	public abstract AsmItem adde(int lineNo, AnnotatedElement ele);

	public int size() {
		return list.size();
	}

	public AsmItem get(int ind) {
		return list.get(ind);
	}
	/**
	 * 	元素类型有：属性 | 方法 | 类
	 */
	public static class AsmItem {
		public AnnotatedElement ele;

		public AsmItems dad;

		public final int index, lineNo;

		public int maxDepth = 999;

		public boolean isFieldDefine = false;

		public FieldRange fieldRange;
		public String key() {
			return ele.toString();
		}

		public String toString() {
			if (dad == null || dad.fromItem == null) {
				return this.toString00();
			}
			return "\n" + dad.fromItem.toString00() + "\n ~>" + this.toString00();
		}

		public AsmItem(AnnotatedElement ele) {
			this(null, ele, -1);
		}

		public AsmItem(AsmItems dad, AnnotatedElement ele, int lineNo) {
			this.dad = dad;
			this.lineNo = lineNo;
			this.index = dad == null ? -1 : dad.size();
			this.ele = ele;
			if (ele instanceof Field) {
				Field fd = (Field) ele;
				if (fd.getName().equals("UseLog"))
					Ca.pause();
			} else if (ele instanceof Method) {
				Method md = (Method) ele;
				if (md.getName().equals("contains"))
					Ca.pause();
			}
		}

		public Class<?> getCls() {
			if (ele instanceof Executable) {
				Executable method = (Executable) ele;
				return method.getDeclaringClass();
			} else if (ele instanceof Field) {
				Field method = (Field) ele;
				return method.getDeclaringClass();
			} else {
				return ((Class<?>) ele);
			}
		}

		String toString00() {
			String name = ele.toString();
			if (ele instanceof Executable) {
				Executable method = (Executable) ele;
				//method.getDeclaringClass().getSimpleName() 
				name = Class9.strOfMethod(method);
			} else if (ele instanceof Field) {
				Field method = (Field) ele;
				name = method.getDeclaringClass().getSimpleName() + "." + method.getName();
			} else {
				name = ((Class<?>) ele).getSimpleName();
			}
			return index + ",NL:" + lineNo + "," + name;
		}

		@SuppressWarnings("unchecked")
		public <T> T get() {
			return (T) ele;
		}
	}
	static int GenID;
}
