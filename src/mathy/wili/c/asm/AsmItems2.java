package mathy.wili.c.asm;
import java.lang.reflect.AnnotatedElement;
/**
 * 
 * @author weila 2021年3月1日
 */
public class AsmItems2 extends AsmItems {
	public AsmItems2() {
		super(null);
	}

	@Override
	public AsmItem adde(int lineNo, AnnotatedElement ele) {
		AsmItem ret;
		list.add(ret = new AsmItem(this, ele, lineNo));
		return ret;
	}

	@Override
	public int size() {
		return list.size();
	}
}
