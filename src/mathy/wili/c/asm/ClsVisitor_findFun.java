package mathy.wili.c.asm;
import java.lang.reflect.Method;
import mathy.wili.c.MiscFilter.ClsFilter;
import net.bytebuddy.jar.asm.ClassVisitor;
import net.bytebuddy.jar.asm.MethodVisitor;
import net.bytebuddy.jar.asm.Opcodes;
/**
 * 
 * @author weila 2021年3月2日
 */
class ClsVisitor_findFun extends ClassVisitor {
	ClsFilter filt;

	AsmCont cont;

	AsmItems toList;

	String funName;

	Class<?>[] funParamTypes;
	public ClsVisitor_findFun(Method method, ClsFilter filt, AsmCont cont) {
		super(Opcodes.ASM5, null);
		this.cont = cont;
		if (method != null) {
			this.funName = method.getName();
			this.funParamTypes = method.getParameterTypes();
		}
		this.filt = filt;
		this.toList = cont.toList;
	}

	@Override
	public void visit(int version, int access, String name, String signature, String superName, String[] interfaces) {
		if (cv != null) {
			cv.visit(version, access, name, signature, superName, interfaces);
		}
	}

	@Override
	public MethodVisitor visitMethod(int access, String name, String desc, String signature, String[] exceptions) {
		if (cv != null)
			cv.visitMethod(access, name, desc, signature, exceptions);
		if (name.equals(Asm9.CLINIT)) {
			//Ca.asert(false, "never");
			return new FunVisitor(null, filt, cont);
		} else if (name.equals(Asm9.INIT)) {
			return new FunVisitor(null, filt, cont);
		} else if (this.funName == null || this.funName.equals(name)) {
			String[] array = name.split("\\$");
			if (array.length > 1) {
				if (array[array.length - 1].matches("[0-9]+")) {
					return new FunVisitor(null, filt, cont);
				}
			}
			return new FunVisitor(null, filt, cont);
		} else {
			return null;
		}
	}
}