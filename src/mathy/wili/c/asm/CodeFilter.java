package mathy.wili.c.asm;
/**
 * 
 * @author weila 2021年3月7日
 */
public interface CodeFilter {
	boolean doCode(int opcode, Object... infos);
}
interface PutFieldFilter extends CodeFilter {
	/**
	 * see com.sun.org.apache.bcel.internal.Const.PUTSTATIC
	 */
	static final int PUTSTATIC = 179;

	static final int PUTFIELD = 181;

	static final int GETFIELD = 180;

	static final int GETSTATIC = 178;
}