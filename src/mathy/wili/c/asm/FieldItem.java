package mathy.wili.c.asm;
import java.io.Serializable;
/**
 * 
 * @author weila 2021年3月8日
 */
public class FieldItem implements Serializable {
	public Class<?> cls;

	public String field;
	public String toString() {
		return cls.getSimpleName() + "." + field;
	}
}
