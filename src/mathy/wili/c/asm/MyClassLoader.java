package mathy.wili.c.asm;
public class MyClassLoader extends ClassLoader {
	public Class<?> defineClassPublic(String name, byte[] b, int off, int len) throws ClassFormatError {
		Class<?> clazz = defineClass(name, b, off, len);
		return clazz;
	}
}
