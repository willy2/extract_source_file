package mathy.wili.c.types;
/**
 * Booolean return value, this may help trace.<br>
 * 最多可表达4种状态：正、负、零、未知。便于debug, log.
 */
public class IsRet {
	public final long id = ++GenID;

	/**
	 * 0零|1正|-1负|2未知
	 */
	public static final byte N0 = 0, N1 = 1, N2 = 2;

	/**
	 * flag: 0零|1正|-1负|2未知
	 */
	public byte flag;
	public String toString() {
		return "i:" + id + ",f:" + flag;
	}
	public static long GenID;
}
