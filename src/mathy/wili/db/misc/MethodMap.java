package mathy.wili.db.misc;
import java.util.HashMap;
import java.util.Map;
import mathy.c.Ca;
/**
 * 
 * @author weila 2021年3月10日
 */
@SuppressWarnings("rawtypes")
public class MethodMap {
	public Map<String, Class[]> map = new HashMap<>();
	public MethodMap add(String name, Class... cc) {
		if (map.put(name, cc) != null)
			Ca.asert(false, name);
		return this;
	}
}
