package mathy.wili.extract_srcFile.java;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import mathy.c.Ca;
import mathy.wili.c.Str9;
/**
 *   field|method|class|annotaion|notation
 * @author weila 2021年2月19日
 */
class ClsMemberRange implements Serializable {
	private static final long serialVersionUID = 1L;

	public final int aId = ++GenID;

	public RootOfClsMember root;

	public final int aLineNo, aStart;

	int lastLineNo = -1;

	public boolean visited = false;

	/**
	 *  declaring class.
	 */
	public final ClsRange clsRange;

	int depth, bodyStart, aEnd;

	String clsName;

	int memIndex;

	String msg;

	List<ClsRange> sons = new ArrayList<>(5);
	{
		if (aId == 99)
			Ca.pause();
	}
	public String toString() {
		String st = toMemberString();
		return st + ":" + root.fileSt.substring(aStart, aEnd).trim();
	}

	public String toMemberString() {
		if (this.aLineNo == 99)
			Ca.pause();
		String st = Str9.repeat('\t', depth) + "D" + depth + "," + memIndex + ",NL" + aLineNo//
				+ "," + this.getClass().getSimpleName() + "[" + aStart + "~" + aEnd + "]";
		return st + "," + (visited ? "visited," : "nv,");
	}

	protected ClsMemberRange(ClsRange crange, int lineNo, int start) {
		this.msg = "noParam_constructor";
		this.aLineNo = lineNo;
		this.aStart = start;
		this.clsRange = crange;
	}

	protected ClsMemberRange(int lineNo) {
		this.msg = "for_search";
		this.aLineNo = lineNo;
		this.aStart = -1;
		this.clsRange = null;
	}

	protected ClsMemberRange(RootOfClsMember root, Matcher mat) {
		this.msg = "method|class|anno|notation";
		this.root = root;
		this.aStart = mat.start();
		this.aLineNo = root.lineNoAt(aStart);
		if (root.curClsRange != null) {
			this.clsRange = root.curClsRange;
			this.depth = root.curClsRange.depth + 1;
		} else if (this instanceof ClsRange) {
			this.clsRange = (ClsRange) this;
		} else {
			this.clsRange = null;//比如说，类上的注解，暂未得到类信息
		}
	}

	protected ClsMemberRange(RootOfClsMember root, int start, int end) {
		this.msg = "field";
		this.root = root;
		if (start >= end)
			Ca.asert(false, root.clazz);
		this.aStart = start;
		this.aEnd = end;
		this.aLineNo = root.lineNoAt(aStart);
		this.clsRange = root.curClsRange;
		if (root.curClsRange != null)
			;
		this.depth = root.curClsRange.depth + 1;
	}

	public static ClsMemberRange findPreSibling(int fromInd, List<ClsMemberRange> list, ClsMemberRange curMember,
			Predicate<ClsMemberRange> pred) {//indexOfPreviousClsMemberRange
		int depth = curMember.depth;
		for (int i = fromInd; i >= 0; i--) {
			ClsMemberRange item = list.get(i);
			if (depth > item.depth) {
				return item;
			} else if (depth < item.depth) {
				return null;
			} else {
				if (pred.test(item))
					return item;
			}
		}
		return null;
	}
	public static final Comparator<ClsMemberRange> CompByLineNo = new Comparator<ClsMemberRange>() {
		@Override
		public int compare(ClsMemberRange o1, ClsMemberRange o2) {
			return o1.aLineNo - o2.aLineNo;
		}
	};
	public void setVisited() {
		this.visited = true;
		if (this instanceof ClsRange || this.clsRange == null)
			return;
		if (this instanceof MethodRange) {
		}
		this.clsRange.setVisited();
	}
	static int GenID;
}
class InitBlockRange extends ClsMemberRange {
	private static final long serialVersionUID = 1L;

	public final boolean isStatic;
	public InitBlockRange(RootOfClsMember root, boolean isStatic, int start, int end) {
		super(root, start, end);
		String st = root.fileSt.substring(start, end);
		this.isStatic = isStatic;
		if (isStatic || st.contains("static"))
			Ca.pause();
		else
			Ca.pause();
	}
}
class AnnotationRange extends ClsMemberRange {
	private static final long serialVersionUID = 1L;
	public AnnotationRange(RootOfClsMember root, Matcher mat) {
		super(root, mat);
		this.aEnd = mat.end();
	}
}
class NotationRange extends ClsMemberRange {
	protected NotationRange(ClsRange crange, int lineNo, int start) {
		super(crange, lineNo, start);
		// TODO Auto-generated constructor stub
	}
	private static final long serialVersionUID = 1L;
}