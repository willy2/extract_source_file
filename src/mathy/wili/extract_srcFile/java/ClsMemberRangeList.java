package mathy.wili.extract_srcFile.java;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import mathy.c.Ca;
import mathy.wili.c.Misc9;
/**
 * 
 * @author weila 2021年2月21日
 */
class ClsMemberRangeList implements Serializable {
	private static final long serialVersionUID = 1L;

	final List<ClsMemberRange> list = new ArrayList<>();

	final Class<?> cls;
	public String toString() {
		String st = Misc9.concatList(list, "\n");
		return st;
	}

	public ClsMemberRangeList(Class<?> cls) {
		this.cls = cls;
	}

	public void add(ClsMemberRange mem) {
		mem.memIndex = list.size();
		if (list.size() > 0) {
			ClsMemberRange pre = list.get(list.size() - 1);
			if (pre.aLineNo > mem.aLineNo || pre.aStart >= mem.aStart)
				Ca.asert(false, mem);
		}
		Ca.pause();
		list.add(mem);
	}

	public ClsMemberRange get(int ind) {
		return list.get(ind);
	}

	public int size() {
		return list.size();
	}

	public int indexOfMember_byLineNo(int lineNo) {
		return Collections.binarySearch(this.list, new MethodRange(lineNo), ClsMemberRange.CompByLineNo);
	}
}
