package mathy.wili.extract_srcFile.java;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javassist.ClassPool;
import mathy.c.Ca;
import mathy.c.LogConf;
import mathy.wili.c.File9;
/**
 * 	从执行路径上提取使用过的类和类方法。
 */
public class CodeExtract {
	static final File DATA_FILE = new File(LogConf.ConfRoot, CodeExtract.class.getSimpleName());

	static final Map<Class<?>, RangesOfClsMember> DATA_MAP = new HashMap<>();

	public static final List<RangesOfClsMember> DATA_LIST = new ArrayList<>(DATA_MAP.values());

	static final ClassPool ClsPool = ClassPool.getDefault();
	public static void saveToFile() {
		if ("".isEmpty())
			return;
		if (!CodeExtract.saveToFile)
			return;
		if (DATA_MAP == null || DATA_MAP.isEmpty())
			return;
		if (savedVersionId == curVersionId)
			return;
		File9.writeObject(DATA_MAP, DATA_FILE, -1);
		savedVersionId = curVersionId;
		Ca.debug(1, "\n" + DATA_MAP.size() + " files, CodeExtract.saveToFile: " + DATA_FILE.getAbsolutePath());
	}
	public static boolean saveToFile = "".isEmpty();

	public static int savedVersionId, curVersionId;
}
