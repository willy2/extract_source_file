package mathy.wili.insertLog;
/**
 * @author weila 2021年2月11日
 */
public interface BufferedLog {
	BufferedLog log(Object msg);
}
