package mathy.wili.insertLog;
import java.io.File;
import java.util.TreeSet;
import _a.Config26;
/**
 * 
 * @author weila 2021年2月16日
 */
public class JarFilePackageNameSet extends FileConfig {
	/**
	 * 
	 * @param name
	 * @return
	 */
	public static boolean contains(String name) {
		inst();
		return INST.set.contains(name);
	}
	final TreeSet<String> set = new TreeSet<>();

	private static JarFilePackageNameSet INST;
	private static JarFilePackageNameSet inst() {
		if (INST != null)
			return INST;
		INST = new JarFilePackageNameSet();
		return INST;
	}

	public String toString() {
		return set.size() + set.toString();
	}

	private JarFilePackageNameSet() {
		super(new File(Config26.ConfRoot, JarFilePackageNameSet.class.getSimpleName()), 10);
		this.readConfigFile();
	}

	public static void add(String name) {
		inst();
		INST.set.add(name);
		INST.updated();
	}

	@Override
	protected void setInst(FileConfig inst) {
		INST = (JarFilePackageNameSet) inst;
	}
	private static final long serialVersionUID = 1L;
}
