package mathy.wili.insertLog;
import java.util.Arrays;
/**
 * 
 * @author weila 2021年2月15日
 */
public class LogConst {
	public static void asert(boolean is, Object... msgs) {
		if (!is)
			throw new RuntimeException(Arrays.asList(msgs).toString());
	}
}
